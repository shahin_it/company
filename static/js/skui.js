const Toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: true,
    timer: 5000
});

if (window.jQuery) {
    $ = window.jQuery;

    Object.defineProperty(String.prototype, "jq", {
        get: function () {
            return $("" + this)
        }
    });
    Object.defineProperty(Element.prototype, "jq", {
        get: function () {
            return $(this)
        }
    });

    $.fn.extend({
        loader: function (show) {
            if (show === false) {
                this.mask(false);
            } else {
                let l = this.mask().addClass("loader");
                if (this.is("body")) {
                    l.css({position: "fixed"})
                }
            }
            return this;
        },
        mask: function (show) {
            if (show == false) {
                this.find(".skui-mask").remove();
            } else {
                this.append('<div class="skui-mask">\
                                <div class="d-flex justify-content-center">\
                                  <div class="spinner-border text-info" style="width: 3rem; height: 3rem;" role="status">\
                                    <span class="sr-only">Loading...</span>\
                                  </div>\
                                </div>\
                            </div>');
            }
            return this.find(".skui-mask");
        },
        serializeObject: function () {
            let o = {};
            let a = (this.is("form") ? this : this.find(":input")).serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!$.isArray(o[this.name])) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        },
        updateUi: function () {
            updateUi.call(this);
            $(document).triggerHandler("update-ui", [this])
        }
    });

    var skui = {
        dataTable: function (container, data, config) {
            let _self = this;
            if (!container.is(".skui-data-table")) {
                return;
            }

            let filterForm = container.find(".filter-form");
            data = container.cacheData = $.extend({
                offset: 0,
                max: app.maxResult,
                searchText: ""
            }, data, filterForm.serializeObject());

            config = $.extend({
                url: "#",
                beforeLoad: function () {
                },
                afterLoad: function () {
                }
            }, container.data(), config);
            container.on("click", ".table-reload", function () {
                container.reload();
            });
            container.on("click", ".filter-form button", function () {
                if (this.jq.is(".clear-button")) {
                    filterForm[0].reset();
                }
                container.reload(filterForm.serializeObject());
            });
            container.on("keypress", ".filter-form input", function (e) {
                if (e.which == 13) {
                    container.reload(filterForm.serializeObject());
                }
            });

            function createEditItem(data) {
                data = $.extend({}, data)
                let url = data.url;
                delete data.url;
                let _popup = skui.editPopup(app.baseUrl + url, data, {
                    size: "modal-lg",
                    title: "Create/Edit " + (config.feature ? config.feature : ''),
                    loaded: function (popup, body) {
                        container.trigger("popupLoaded", arguments);
                    },
                    preSubmit: function () {
                        if (container.trigger("popupPreSubmit", arguments) == false) {
                            return false
                        }
                    },
                    success: function () {
                        container.reload();
                        container.trigger("popupSubmit", arguments);
                    }
                });
            }

            container.on("click", ".add-new", function () {
                createEditItem($(this).data());
            });
            container.on("click", ".action-navigator [data-action]", function () {
                let $this = this.jq;
                let data = $.extend({}, ($this.parent().data() || {}), $this.data());
                let action = data.action;
                delete data.action
                container.trigger("onActionClick", [action, data]);
                container.trigger("onAction:" + action, [data]);
            });
            container.on("onActionClick", function (evt, action, data) {
                switch (action) {
                    case 'edit':
                        createEditItem(data);
                        break;
                    case "delete":
                    case "remove":
                        let url = data.url;
                        delete data.url
                        let ask = "Are you confirm to remove ?";
                        if (data.name) {
                            ask = 'Are you confirm to remove "<b>' + data.name + '</b>" ?';
                        }
                        skui.confirm(ask, function () {
                            container.loader();
                            skui.ajax({
                                url: app.baseUrl + url,
                                dataType: "json",
                                data: data,
                                response: function () {
                                    container.loader(false);
                                },
                                success: function (resp) {
                                    if (resp && resp.message) {
                                        if (resp.status == "success") {
                                            container.reload();
                                        }
                                        skui.notify(resp.message, resp.status);
                                    }
                                }
                            })
                        });
                        break;
                }
            });

            container.reload = function (reloadData) {
                if (config.beforeLoad.apply(this, [data]) == false) {
                    return;
                }
                let reqData = $.extend(data, reloadData);
                container.loader();
                skui.ajax({
                    method: "post",
                    url: app.baseUrl + config.url,
                    data: reqData,
                    dataType: "html",
                    response: function (resp) {
                        container.loader(false);
                    },
                    success: function (resp) {
                        resp = resp.jq;
                        if (resp.length) {
                            $.extend(container.cacheData, reloadData);
                            let tableBody = container.find(".skui-table");
                            tableBody.html(resp.find(".skui-table").html());
                            tableBody.updateUi();
                            container.find(".filter-form").prev("input").val(reqData.searchText);
                            container.find(".pagination").replaceWith(resp.find(".pagination"));
                            _self.pagination(container);
                            config.afterLoad.apply(this, arguments);
                        }
                    }
                });
            };
            _self.pagination(container);

            return container;
        },
        paginatedPage: function (container, data, config) {
            let _self = this;
            if (!container.is(".skui-paginated-page")) {
                return;
            }

            let filterForm = container.find(".filter-form");
            data = container.cacheData = $.extend({
                page: 0,
                max: app.maxResult,
                searchText: ""
            }, data, filterForm.serializeObject());

            config = $.extend({
                url: "#"
            }, container.data(), config);
            container.on("click", ".filter-form button", function () {
                if (this.jq.is(".clear-button")) {
                    filterForm[0].reset();
                }
                container.reload(filterForm.serializeObject());
            });
            container.on("keypress", ".filter-form input", function (e) {
                if (e.which == 13) {
                    container.reload(filterForm.serializeObject());
                }
            });

            _self.pagination(container);

            container.reload = function (reloadData) {
                let reqData = $.extend(data, reloadData);
                container.loader();
                $.extend(container.cacheData, reloadData);
                location.href = app.baseUrl + config.url + "?page=" + data.page;
            };
        },
        pagination: function (container) {
            let pagination = container.find(".pagination");

            const count = parseInt(pagination.data("count"));
            const offset = parseInt(pagination.data("offset"));
            let data = $.extend({
                offset: offset ? offset : 0,
                page: 0,
                max: app.maxResult
            }, container.cacheData);
            if (!count) {
                return
            }
            if (data.max == count) {
                data.offset = 0
            }
            return pagination.twbsPagination({
                startPage: (data.offset / data.max) + 1,
                visiblePages: 3,
                first: "«",
                prev: "‹",
                next: "›",
                last: "»",
                initiateStartPageClick: false,
                totalPages: Math.ceil(count / data.max),
                onPageClick: function (evt, _offset) {
                    data.page = _offset - 1;
                    data.offset = (_offset - 1) * data.max;
                    container.reload(data);
                }
            });
        },
        ajax: function (settings) {
            if (!settings) {
                return;
            }
            let response = settings.response
                , success = settings.success
                , error = settings.error;
            delete settings.response;
            delete settings.success;
            delete settings.error;
            $.extend(settings.data, {
                ajax: true
            })
            let _settings = $.extend({
                async: true,
                dataType: "json",
                success: function (resp) {
                    response && response.call(this);
                    if (skui.silentLogin(resp)) {
                        success && success.apply(this, arguments);
                    }
                },
                error: function (errorObj) {
                    response && response.call(this);
                    if (skui.silentLogin(errorObj.responseText)) {
                        error && error.apply(this, arguments);
                    }
                }
            }, settings);
            return $.ajax(_settings);
        },
        ajaxForm: function (form, settings) {
            if (!form.length || !settings) {
                return;
            }
            let response = settings.response;
            let beforeSubmit = settings.beforeSubmit;
            let success = settings.success;
            let error = settings.error;
            delete settings.response;
            delete settings.beforeSubmit;
            delete settings.success;
            delete settings.error;
            let _settings = $.extend({
                type: "POST",
                dataType: "json",
                beforeSubmit: function (arr, $form, options) {
                    arr.push({
                        name: "ajax",
                        value: true
                    })
                    if (form.triggerHandler("beforeSubmit") == false) {
                        return false;
                    }
                    if (beforeSubmit && beforeSubmit.apply(this, arguments) == false) {
                        return false
                    }
                },
                success: function (resp, type) {
                    response && response.call(this);
                    if (skui.silentLogin(resp)) {
                        success && success.apply(this, arguments);
                    }
                },
                error: function (errorObj) {
                    response && response.call(this);
                    if (skui.silentLogin(errorObj.responseText)) {
                        error && error.apply(this, arguments);
                    }
                }
            }, settings);

            return form.ajaxForm(_settings);
        },
        notify: function (message, type) {
            type = type || "info"
            if (window.Swal) {
                this.sweetAlert.apply(this, arguments);
                return
            }
            let sign = {error: "exclamation-triangle", info: "info-circle", success: "check-circle"};
            let alert = $('<div class="skui-alert alert alert-' + (type == 'error' ? 'danger' : type) + ' alert-dismissible fade show" role="alert">\
			  <strong class="pr-2 fas fa-' + sign[type] + '"></strong>' + message + '.\
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>\
			</div>');
            $("body").prepend(alert);
            setTimeout(function () {
                alert.alert('close');
            }, 8000);
            return alert.alert();
        },
        sweetAlert: function (message, type) {
            Toast.fire({
                icon: type,
                title: type == "success" ? "Good job!" : "Opps!",
                text: message
            });
        },
        editPopup: function (url, data, config) {
            let content = "";
            config = $.extend({
                class: "",
                title: "",
                width: 600,
                size: "modal-lg",
                preSubmit: undefined,
                loaded: undefined
            }, config);
            config.size = config.size == "sm" ? "modal-sm" : (config.size == "md" ? "modal-md" : config.size)
            data = $.extend({
                id: undefined
            }, data);
            if (typeof url != "string") {
                content = url;
            }
            let popup = $('<div class="modal fade skui-popup skui-edit-popup ' + config.class + '" tabindex="-1" role="document" aria-hidden="true">\
            <div class="modal-dialog ' + config.size + '" role="document">\
            	<div class="modal-content">\
                   <div class="modal-header">\
                   		<h5 class="modal-title popup-title">' + config.title + '</h5>\
                        <span class="close fas fa-window-close link" data-dismiss="modal"></button>\
                    </div>\
                    <div class="modal-body popup-body"><div class="popup-message text-center text-danger"></div></div>\
                    <div class="modal-footer popup-footer">\
                        <button type="button" class="btn btn-sm btn-warning" data-dismiss="modal">Cancel</button>\
                    </div>\
                    </div>\
                </div>\
            </div>');

            $("body").append(popup);

            popup.on("show.bs.modal", function () {
                let body = popup.find(".popup-body");
                if (content && content.length) {
                    popupLoaded(content);
                } else {
                    body.loader();
                    skui.ajax({
                        url: url,
                        type: "GET",
                        data: data,
                        dataType: "html",
                        response: function () {
                            body.loader(false);
                        },
                        success: function (resp) {
                            popupLoaded(resp.jq);
                        }
                    });
                }

                function popupLoaded(content) {
                    let title = content.find(".form-title");
                    if (title.length) {
                        popup.find(".popup-title").html(title);
                    } else {
                        popup.find(".popup-title").html(config.title);
                    }
                    let form = content.filter("form");
                    if (!form.length) {
                        form = content.find("form:first");
                    }
                    form.attr("id", "create-edit-form-submit");
                    popup.find(".popup-footer").prepend(content.find(".form-submit").attr("form", "create-edit-form-submit"));
                    body.append(content);
                    content.updateUi();
                    popup.modal('handleUpdate');

                    skui.ajaxForm(form, {
                        type: "POST",
                        dataType: "json",
                        beforeSubmit: function (arr, $form, options) {
                            body.loader();
                            if (config.preSubmit) {
                                return config.preSubmit.apply(this, arguments);
                            }
                        },
                        response: function () {
                            body.loader(false);
                            if (config.response) {
                                config.response.apply(this);
                            }
                        },
                        success: function (resp, type) {
                            if (resp && resp.message) {
                                skui.notify(resp.message, (resp.success ? 'success' : undefined));
                            }
                            if (config.success) {
                                config.success.apply(this, arguments);
                            }
                            popup.modal("hide");
                        },
                        error: function (resp) {
                            popup.find('.popup-message').html(resp.responseJSON.message);
                        }
                    });
                    config.loaded && config.loaded.apply(popup, body);
                }
            });
            popup.on("hidden.bs.modal", function () {
                let _popup = this.jq;
                config.close && config.close.apply(this, arguments);
                _popup.removeData();
                _popup.modal('dispose');
                _popup.remove();

            });
            popup.modal({
                backdrop: true,
                keyboard: true,
                show: true
            });
            return popup;
        },
        confirm: function (message, yes, no) {
            if (window.Swal) {
                this.confirmSweetAlert.apply(this, arguments);
                return
            }
            let popup = $('<div class="skui-popup skui-confirm modal fade" tabindex="-1" role="document" aria-hidden="true">\
            <div class="modal-dialog modal-md" role="document">\
            	<div class="modal-content">\
                   <div class="modal-header">\
                        <h5 class="modal-title popup-title">Confirm!</h5>\
                        <span class="close fas fa-window-close link" data-dismiss="modal"></span>\
                    </div>\
                    <div class="modal-body"><p>' + message + '</p></div>\
                    <div class="modal-footer">\
                    	<button type="button" class="btn btn-sm btn-primary yes" data-dismiss="modal">Yes</button>\
                        <button type="button" class="btn btn-sm btn-warning no" data-dismiss="modal">No</button>\
                    </div>\
                    </div>\
                </div>\
            </div>');

            popup.on("click", ".modal-footer", function (evt) {
                if ($(evt.target).is(".yes")) {
                    yes && yes();
                } else {
                    no && no();
                }
                popup.modal('hide');
            })

            popup.on("hidden.bs.modal", function (evt) {
                popup.removeData();
                popup.modal('dispose');
                popup.remove();
            });

            popup.modal({
                backdrop: true,
                keyboard: false,
                focus: true,
                show: true
            });
            return popup;
        },
        confirmSweetAlert: function (message, yes, no) {
            Swal.fire({
                title: 'Are you sure?',
                text: message,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, do it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    yes && yes();
                } else {
                    no && no();
                }
            })
        },
        imageInput: function (inputControl) {
            let fileInput = inputControl.find("input[type=file]");
            let imgPrev = inputControl.find(".skui-image-preview");

            fileInput.on("change", function (evt) {
                let files = evt.target.files;
                if (!files.length) {
                    return
                }
                let maxSize = fileInput.data("maxSize");
                if (maxSize && files[0].size > maxSize) {
                    alert("Max allowed size is " + maxSize + "!");
                    this.value = "";
                    return;
                }
                ;
                let reader = new FileReader();
                reader.onload = function (evt) {
                    imgPrev.attr("src", evt.target.result)
                }
                reader.readAsDataURL(files[0])
            })
        },
        accordion: function (panel) {
            if (!panel.is(".skui-accordion-panel")) {
                return
            }
            panel.expand = function (label) {
                panel.find(".skui-accordion-label").removeClass("expanded");
                panel.find(".skui-accordion-item").removeClass("expanded").hide();
                label.addClass("expanded").find(".accordion-toggle").toggleClass("fa-minus fa-plus");
                label.next(".skui-accordion-item").addClass("expanded").show();
            }
            panel.collapse = function (label) {
                label.removeClass("expanded").find(".accordion-toggle").toggleClass("fa-minus fa-plus");
                label.next(".skui-accordion-item").removeClass("expanded").hide();
            }
            panel.expand(panel.find(".skui-accordion-label:first"));
            panel.on("click", ".skui-accordion-label", function (evt) {
                let label = this.jq;
                if (label.is(".leaf")) {
                    return;
                }
                if (label.is(".expanded")) {
                    panel.collapse(label);
                } else {
                    panel.expand(label);
                }
            })
            return panel;
        },
        toggle: function (container) {
            let inputs = container.find("[data-toggle-target]");
            if (!inputs.length) {
                return
            }

            inputs.each(function () {
                let input = this.jq;
                let target = input.attr("data-toggle-target");
                let selected = container.find("[class^='" + target + "-'], [class*=' " + target + "-']");
                if (!selected.hasClass(target + "-" + input.val())) {
                    selected.hide();
                }

                if (input.is("select")) {
                    input.change(function () {
                        container.find("[class^='" + target + "-'], [class*=' " + target + "-']").hide().find("input, select, textarea").attr("disabled", true);
                        container.find("." + target + "-" + input.val()).show().find("input, select, textarea").removeAttr("disabled");
                    }).trigger("change")
                }
            })
        },
        silentLogin: function () {
            try {
                resp = $(resp)
            } catch (ex) {
                resp = $()
            }
            if (resp.is(".silent-login-popup")) {
                skui.editPopup(resp)
                return false
            }
            return true
        },
        modalImage: function (images) {
            images.on("click", function () {
                let img = this.jq;
                let popup = $('<div class="modal popup-image-modal">\n' +
                    '  <span class="close">&times;</span>\n' +
                    '  <img class="modal-content">\n' +
                    '  <div class="caption"></div>\n' +
                    '</div>');
                popup.modal({
                    backdrop: false,
                    keyboard: true,
                });
                popup.find(".modal-content").attr("src", this.src);
                this.alt && popup.find(".caption").text(this.alt);
                popup.find(".close").click(function () {
                    popup.modal('hide');
                    popup.remove();
                });
            });
        },
        getXpath: function (element) {
            var paths = [];  // Use nodeName (instead of localName)
            // so namespace prefix is included (if any).
            for (; element && element.nodeType == Node.ELEMENT_NODE;
                   element = element.parentNode) {
                var index = 0;
                var hasFollowingSiblings = false;
                for (var sibling = element.previousSibling; sibling;
                     sibling = sibling.previousSibling) {
                    // Ignore document type declaration.
                    if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
                        continue;

                    if (sibling.nodeName == element.nodeName)
                        ++index;
                }

                for (var sibling = element.nextSibling;
                     sibling && !hasFollowingSiblings;
                     sibling = sibling.nextSibling) {
                    if (sibling.nodeName == element.nodeName)
                        hasFollowingSiblings = true;
                }

                var tagName = (element.prefix ? element.prefix + ":" : "")
                    + element.localName;
                var pathIndex = (index || hasFollowingSiblings ? "["
                    + (index + 1) + "]" : "");
                paths.splice(0, 0, tagName + pathIndex);
            }

            return paths.length ? "/" + paths.join("/") : null;
        },
        getCssSelector: function (node) {
            if (node.length != 1) throw 'Requires one element.';

            var path;
            while (node.length) {
                var realNode = node[0], name = realNode.localName;
                if (!name) break;
                name = name.toLowerCase();

                var parent = node.parent();
                var siblings = parent.children(name);
                if (siblings.length > 1) {
                    name += ':eq(' + siblings.index(realNode) + ')';
                }
                path = name + (path ? '>' + path : '');
                node = parent;
            }

            return path;
        },
        ingFy: function (str) {
            if (str) {
                let lastChar = str.charAt(str.length - 1).toLowerCase();
                switch (lastChar) {
                    case 'e':
                        str = str.substr(str.length - 1) + 'ing';
                        break;
                    case 'l':
                    case 't':
                        str += lastChar + 'ing';
                        break;
                    default:
                        str += 'ing';
                        break;
                }
            }
            return str;
        },
        renderOverlay: function (data, parent) {
            let _self = this;
            if (typeof (parent) == 'string') {
                parent = $(parent);
            } else {
                parent = $('body');
            }
            let overlay = $('<div id="skui-overlay" class="p-5">' +
                '<span class="close close-overlay text-danger text-xl fas fa-window-close link"></span></div>');
            let _data = {};

            if (typeof (data) == 'string') {
                _data.url = data
            } else if (data instanceof Object && !(data instanceof $)) {
                $.extend(_data, data);
            }

            if (data instanceof $) {
                appendOverlay(data);
            } else {
                parent.loader(true);
                _self.ajax({
                    method: "get",
                    url: app.baseUrl + _data.url,
                    data: {},
                    dataType: "html",
                    response: function (resp) {
                        parent.loader(false);
                    },
                    success: function (resp) {
                        appendOverlay(resp);
                    }
                })
            }

            function appendOverlay(elm) {
                overlay.append(elm);
                parent.append(overlay);
                overlay.updateUi();
                overlay.find('.close-overlay').on('click', function () {
                    overlay.remove();
                })
            }

        },
    };


    function updateUi() {
        let delegate = this;
        delegate.find(".skui-data-table").each(function () {
            skui.dataTable($(this));
        });
        delegate.find('.skui-overlay').each(function () {
            let $this = this.jq;
            $this.click(function () {
                let id = $this.parents('tr').data('id') || '';
                skui.renderOverlay($this.data('overlay-url') + `/${id}`);
            });
        });
        skui.paginatedPage(delegate.find(".skui-paginated-page"));
        this.find(".skui-file-chooser").on("change", "input[type=file]:last", function () {
            if (this.value) {
                if ((this.files[0].size / 1024) > (+this.jq.attr("max-size"))) {
                    skui.notify("Max size 2 MB");
                    this.jq.replaceWith(this.jq.val('').clone(true));
                    return false;
                }
                if (!this.jq.is(".single")) {
                    let input = $('<div class="input"><input type="file" name="s_image[]" value="" max-size="2048"><i class="action remove fa fa-times color-red" title="Remove"></i></div>');
                    this.jq.parents(".input").after(input);
                    input.find(".remove").click(function () {
                        this.jq.parents(".input").remove();
                    })
                }
            }
        });
        this.find(".skui-image-chooser").each(function () {
            skui.imageInput(this.jq);
        });
        this.find(".skui-accordion-panel").each(function () {
            skui.accordion(this.jq);
        })
        skui.toggle(this);
        let form = this.find(".ajax-submit");
        skui.ajaxForm(form, {
            type: "POST",
            dataType: "json",
            beforeSubmit: function (arr, $form, options) {
                form.loader();
            },
            response: function () {
                form.loader(false);
            },
            success: function (resp, type) {
                if (resp && resp.message) {
                    skui.notify(resp.message, resp.status);
                }
                if (resp.redirect) {
                    location.href = resp.redirect;
                }
            }
        });

        delegate.find('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
            let $el = $(this);
            let $parent = $(this).offsetParent(".dropdown-menu");
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            let $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');

            $(this).parent("li").toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                $('.dropdown-menu .show').removeClass("show");
            });

            if (!$parent.parent().hasClass('navbar-nav')) {
                $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
            }

            return false;
        });

        delegate.find("#top-link").click(function () {
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        });
        delegate.find(".random-color").each(function () {
            let randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            $(this).css({"color": randomColor})
        });
        delegate.find(".random-bg-color").each(function () {
            let randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            $(this).css({"background-color": randomColor})
        });
        delegate.find(".random-border-color").each(function () {
            let randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            $(this).css({"border-color": randomColor})
        });
        skui.modalImage(delegate.find(".popup-image"));
    }

    $(function () {
        let body = $("body");
        app.maxResult = 10;
        body.updateUi();
    });


} else {
    console.log("jQuery missing!");
}
