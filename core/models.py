import operator
from functools import reduce

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q
from django.utils import timezone

from core import constant


class ModelManager(models.Manager):

    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except ObjectDoesNotExist:
            return None

    def find_all(self, query_dic: dict = {}, trash_only=False):
        result = self.none()
        fields = self.model.filter_item
        search_str = query_dic.get('table_search')

        if search_str:
            arg_list = []
            for search in search_str.split(' '):
                for field in fields:
                    arg_list.append(Q(**{field + '__icontains': search}))
            result = self.filter(reduce(operator.or_, arg_list), is_in_trash=trash_only)
        else:
            result = self.filter(is_in_trash=trash_only)

        return result


class ModelBase(models.Model):
    objects = ModelManager()
    created = models.DateTimeField(null=True, auto_now_add=True)
    updated = models.DateTimeField(null=True, auto_now=True)

    edit_item = '__all__'
    list_item = []
    filter_item = []

    class Meta:
        abstract = True


class Settings(ModelBase):
    type = models.CharField(max_length=100)
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        unique_together = ('type', 'key')

    @staticmethod
    def get_config(type, key=None):
        config = {}
        if key:
            return Settings.objects.get(type=type, key=key).value
        for setting in Settings.objects.filter(type=type):
            config[setting.key] = setting.value
        return config

    # @transaction.atomic
    @staticmethod
    def init():
        static_config = constant.APP_CONFIG
        settings = []
        for type in static_config:
            if not Settings.objects.filter(type=type):
                config = static_config[type]
                for key in config:
                    settings.append(Settings(type=type, key=key, value=config[key]))
        Settings.objects.bulk_create(settings)

    @property
    def name(self):
        return self

    def __str__(self):
        return "%s->%s" % (self.type, self.key)


class Navigation(ModelBase):
    title = models.CharField(null=True, max_length=100)
    url = models.CharField(max_length=250, unique=True)
    css_class = models.CharField(max_length=100, null=True, blank=True)
    parent = models.ForeignKey('Navigation', null=True, blank=True, on_delete=models.CASCADE)
    is_new = models.BooleanField(default=False)
    is_in_trash = models.BooleanField(default=False)

    @property
    def child(self):
        return Navigation.objects.filter(parent=self, is_in_trash=False)

    def __str__(self):
        return self.title

    @staticmethod
    def init():
        navs = constant.ADMIN_NAVS
        bulk_nav = []
        for nav in navs:
            if not Navigation.objects.filter(pk=nav.get('id')):
                bulk_nav.append(Navigation(pk=nav.get('id'), is_new=nav.get('is_new', False), title=nav.get('title'),
                                           url=nav.get('url'), css_class=nav.get('css_class'),
                                           parent_id=nav.get('parent')))
        Navigation.objects.bulk_create(bulk_nav)


class Address(models.Model):
    street = models.CharField(max_length=100, null=False, blank=False, verbose_name='Road Name')
    upazila = models.CharField(max_length=30, null=False, blank=False, verbose_name='Upazila')
    district = models.CharField(max_length=30, null=False, blank=False, verbose_name='District')
    country = models.CharField(max_length=30, null=False, blank=False, verbose_name='Country')

    def __str__(self):
        return '%s, %s, %s' % (self.street, self.upazila, self.district)


class User(AbstractUser):
    post = models.CharField(null=True, max_length=10, choices=constant.ROLES)
    email = models.EmailField(unique=True, max_length=50, verbose_name='Email Address')
    mobile = models.CharField(unique=True, max_length=15, null=True)
    sex = models.CharField(max_length=2, choices=constant.SEX, null=True)
    birth_date = models.DateField(null=True)
    date_joined = models.DateField('joined date', default=timezone.now)
    address = models.OneToOneField('Address', null=True, on_delete=models.CASCADE)

    @property
    def name(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        return self.username
