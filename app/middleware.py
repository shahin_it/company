from datetime import datetime

from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin

from app import app_util


def customContext(request):
    context = {
        'base_url': app_util.sleep(lambda: app_util.base_url(request)),
        'app_name': app_util.sleep(lambda: app_util.get_config('app', 'name')),
        'config': app_util.sleep(lambda: app_util.get_config_map())
    }
    return context


class CustomMiddleWare(MiddlewareMixin):

    # One-time configuration and initialization on start-up
    def __init__(self, get_response=None):
        # One-time configuration and initialization on start-up
        self.get_response = get_response
        for ct in ContentType.objects.all():
            model_class = ct.model_class()
            try:
                init_method = getattr(model_class, 'init')
                if init_method:
                    init_method()
            except:
                pass

    def process_request(self, request):
        request._request_time: datetime = timezone.now()
        app_util.set_request(request)

    # def process_view(self, request, view_func, view_args, view_kwargs):
    # Logic executed before a call to view
    # Gives access to the view itself & arguments

    def process_exception(self, request, exception):
        # Logic executed if an exception/error occurs in the view
        print("Exception occurred!")

    def process_template_response(self, request, response):
        return response

    def process_response(self, request, response):
        return response
