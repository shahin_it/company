# pip freeze > requirements.txt
# pip install -r requirements.txt
Django
dj-static
static3
django-cors-headers
djangorestframework
django-cors-middleware
django-widget-tweaks
django-tables2
django-filter
social-auth-app-django
mysqlclient
bangladesh
