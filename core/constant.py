from bangladesh import get_districts

YES_NO = (
    ('', "----Select----"),
    (True, "Yes"),
    (False, "No"),
)
SEX = (
    ('', "----Select----"),
    ('M', "Male"),
    ('F', "Female"),
    ('O', "Others"),
)
ROLES = (
    ('', '----Select----'),
    ('O', 'Operator'),
    ('S', 'Supervisor'),
    ('A', 'Admin'),
)
DELIVERY_STATUS = (
    ('', '----Select----'),
    ('P', 'PENDING'),
    ('D', 'DELIVERED'),
    ('C', 'CANCELED'),
)
PAYMENT_STATUS = (
    ('', '----Select----'),
    ('P', 'PAID'),
    ('U', 'UNPAID'),
    ('PA', 'PARTIAL')
)
DISCOUNT_TYPE = (
    ('', '----Select----'),
    ('F', 'Flat(eg. xx.xTk)'),
    ('P', 'Percent(eg. x%)'),
)

BT_DELIVERY_STATUS = {
    'P': 'warning',
    'D': 'success',
    'C': 'danger',
}

BT_PAYMENT_STATUS = {
    'P': 'success',
    'U': 'danger',
    'PA': 'warning',
}

COUNTRY = {
    'BD': 'Bangladesh',
}
DIVISION = {
    'CHA': 'Chattogram',
}
DISTRICT = {
    'NOA': 'Noakhali',
}
UPAZILA = {
    '1': 'Subarnachar',
}

APP_CONFIG = {
    'app': {
        'name': "Health Care",
        'details': "Service First",
        'logo': "cms/image/logo.png",
        'banner': "cms/image/banner.jpg",
        'address1': "GP-GA 88/1, Middle Badda, Dhaka",
        'address2': None,
        'contact': "+8801700000000",
        'email': "admin@cms.com",
        'web': "http://www.cms.com",
        'item_per_page': "15",
        'date_format': "dd-MM-yyyy",
        'time_format': "hh:mm:ss a",
    },
    'site': {

    },
    'access_control': {},
    'invoice': {
        'deliver_order_within': "2",
        'banner': "cms/image/invoice_banner.jpg",
        'height': '11.69in',
        'width': '8.27in',
        'font_size': '8px',
    },
}

ADMIN_NAV = {
    'clinic': {
        'icon': 'fa-clinic-medical',  # fas fa-tachometer-alt
        'title': 'Clinic',
        'child': {
            'edit_order': {
                'title': 'Add New Invoice',
                'icon': 'fa-plus',
            },
            'order_list': {
                'title': 'Invoice List',
                'icon': 'fa-file-invoice-dollar',
            },
            'product_list': {
                'title': 'Service List',
                'icon': 'fa-vials',
            },
        },
    },
    'settings': {
        'title': 'Settings',
        'icon': 'fa-th',
        'is_new': True,
    }
}

ADMIN_NAVS = [
    {
        'id': 1,
        'title': 'Clinic',
        'url': 'clinic',
        'css_class': 'fa-clinic-medical',
    },
    {
        'id': 2,
        'title': 'Add New Invoice',
        'url': 'edit_invoice',
        'css_class': 'fa-plus',
        'parent': 1,
    },
    {
        'id': 3,
        'title': 'Invoice List',
        'url': 'invoice_list',
        'css_class': 'fa-file-invoice-dollar',
        'parent': 1,
    },
    {
        'id': 4,
        'title': 'Service List',
        'url': 'product_list',
        'css_class': 'fa-vials',
        'parent': 1,
    },
    {
        'id': 5,
        'title': 'Payments',
        'url': 'payment_list',
        'css_class': 'fa-th',
        'is_new': True,
    },
]

GL_HEAD_INCOME = '1'
GL_HEAD_EXPENSES = '0'
GL_HEADS = {
    GL_HEAD_INCOME: 'Income',
    GL_HEAD_EXPENSES: 'Expenses',
}
