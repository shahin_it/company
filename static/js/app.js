if (localStorage.getItem('menu_collapse')) {
    $('body').addClass('sidebar-collapse');
}
$('body').find('[data-widget=pushmenu]').click(function () {
    if ($('body').hasClass('sidebar-collapse')) {
        localStorage.removeItem('menu_collapse');
    } else {
        localStorage.setItem('menu_collapse', true);
    }
});

$('.main-header.navbar').autoHidingNavbar();

$(document).on("update-ui", function (evt, container) {
    if (localStorage.getItem("clicked")) {
        let navLink = container.find("[data-nav='" + localStorage.getItem("clicked") + "']");
        if (navLink.length) {
            navLink.addClass("active");
            let parent = navLink.parents(".nav-item.has-treeview").addClass("menu-open");
            parent.find(".nav-link:first").addClass("active");
        }
    }
    container.find(".nav-item .nav-link").click(function (evt) {
        localStorage.setItem("clicked", $(this).data('nav'));
        let navLink = $(localStorage.getItem("clicked"));
    })
    container.find("button[type=reset]").click(function () {
        $(this).parents("form").trigger("reset").find('select').trigger('change');
    });

    container.find('button[data-action]').click(function () {
        let action = $(this).data('action');
        let type = $(this).parents('table').data('type');
        let name = $(this).parents('tr').data('name');
        let id = $(this).parents('tr').data('id');
        skui.confirm(`Confirm ${action} ${type} '${name}' ?`, function () {
            location.href = app.baseUrl + `${action}-${type}/${id}`
        });
    });

    container.find('.selectfilterstacked').bootstrapDualListbox({
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false,
        moveSelectedLabel: '<i class="fas fa-arrow-right"></i>',
        moveAllLabel: '',
    });

    container.find('.date-picker').each(function () {
        let picker = $(this);
        let format = picker.data('format') || app.date_time_format
        picker.daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            showDropdowns: true,
            autoUpdateInput: picker.attr('required') !== undefined,
            autoApply: true,
            locale: {
                format: format,
            },
        }, function (chosen_date) {
            picker.val(chosen_date.format(format));
        })
    })

    container.find('.select2bs4').select2({
        theme: 'bootstrap4',
    });

    /*container.find(".invoice-print-button").click(function () {
        container.find("#invoice-frame")[0].contentWindow.print();
    });*/

    container.find('.input-mask').inputmask()

    refreshOrderForm(container.find('#order_form'));
    addNewModel(container);
});

function refreshOrderForm(orderForm) {
    let multiSelect = orderForm.find('[data-type=modelmultiplechoicefield]');
    let _discountType = orderForm.find("#id_discount_type");
    let _discount = orderForm.find("#id_discount");
    let _paid = orderForm.find("#id_paid");
    let _paymentStatus = orderForm.find("#id_payment_status");
    let _subTotal = orderForm.find("#id_sub_total");
    let _total = orderForm.find("#id_total");
    multiSelect.on('DOMSubtreeModified', '#bootstrap-duallistbox-selected-list_items', function (evt) {
        let priceData = orderForm.find("#id_items").data("price");
        if (!priceData) {
            return;
        }
        let total = 0;
        $(this).find('option').each(function () {
            total += priceData[$(this).attr("value")];
        });
        let discount = _discount.val();
        if (_discountType.val() == "P") {
            discount = (total * discount) / 100;
        }
        _subTotal.val(total.toFixed(2));
        let grandTotal = total - discount;
        _total.val(grandTotal);
        let paid = 1 * _paid.val();
        if (paid && paid >= grandTotal) {
            _paymentStatus.val("P");
            _subTotal.css({'color': 'green'});
        } else if (paid <= 0) {
            _paymentStatus.val("U");
            _subTotal.css({'color': 'red'});
        } else {
            _paymentStatus.val("PA");
            _subTotal.css({'color': 'blue'});
        }
        _paymentStatus.trigger('change');
    });

    _discountType.add(_discount).add(_paid).change(function () {
        multiSelect.find("#bootstrap-duallistbox-nonselected-list_items, #bootstrap-duallistbox-selected-list_items").trigger("DOMSubtreeModified");
    });

    if (orderForm.find("select[name=items]").attr("readonly")) {
        orderForm.find(".bootstrap-duallistbox-container").find("*").prop("disabled", true);
    }

}

function addNewModel(container) {
    container.find('.add-new-model').click(function () {
        let item = $(this);
        skui.editPopup(app.baseUrl + 'add-new-model', item.data(), {
            success: function (resp) {
                var select2 = item.next('select');
                select2.append('<option selected value="' + resp.id + '">' + resp.name + '</option>');
                select2.trigger('change.select2');
            }
        });
    })
}

/*window.onbeforeunload = function () {
    $(':submit').attr("disabled", "disabled");
};*/

$('form').submit(function () {
    let button = $(this).find(':submit');
    let btnText = button.data('submit-text');
    btnText = btnText || skui.ingFy(button.text());
    button.html('<span class="spinner-border spinner-border-sm"></span> ' + btnText + '...')
        .attr('disabled', 'disabled');
    return true;
});