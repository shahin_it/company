import functools

from django.core.handlers.wsgi import WSGIRequest
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from app import settings
from base_app.models import Settings, Navigation

__CONFIG = []
__CONFIG_MAP = {}
__REQUEST = {'request': None}


def set_request(request):
    __REQUEST['request'] = request


def get_request() -> WSGIRequest:
    return __REQUEST['request']


def sleep(method):
    @functools.wraps(method)
    def sleep(*args, **kwargs):
        method._cache = getattr(method, '_cache', {})
        key = args
        if key not in method._cache:
            method._cache[key] = method(*args, **kwargs)
        return method._cache[key]

    return sleep


def base_url(request):
    return settings.BASE_URL


def get_config_map():
    if len(__CONFIG) == 0:
        __reload_config()
    return __CONFIG_MAP


def get_config(type, key=None):
    if len(__CONFIG) == 0:
        __reload_config()
    config = []
    for row in __CONFIG:
        if key is None:
            if row['type'] == type:
                config.append(row)
        else:
            if row['type'] == type and row['key'] == key:
                return row['value']
    return config


def __reload_config():
    __CONFIG.clear()
    __CONFIG_MAP.clear()
    __CONFIG_MAP['admin_nav'] = Navigation.objects.filter(is_in_trash=False, parent=None)
    for setting in Settings.objects.all():
        __CONFIG.append({'type': setting.type, 'key': setting.key, 'value': setting.value})
        __CONFIG_MAP[setting.type + '_' + setting.key] = setting.value
    return __CONFIG

@receiver(post_save, sender=Settings)
@receiver(post_save, sender=Navigation)
@receiver(post_delete, sender=Settings)
@receiver(post_delete, sender=Navigation)
def update_settings(sender, instance, **kwargs):
    __reload_config()
